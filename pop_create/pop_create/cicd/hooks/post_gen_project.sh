#!/bin/bash

if command -v git
then
  git init || echo "Could not initialize git"
  git add .|| echo "Could not add files"
  if command -v pre-commit
  then
    pre-commit install || echo "pre-commit is not installed"
    SKIP="twine-check" pre-commit run -av || echo $?
    pre-commit autoupdate
  fi
  # Remove git from the temporary directory
  rm -rf .git
  # Remove temp .nox venvs
  rm -rf .nox
  # Remove empty artifacts dir
  rm -rf artifacts
  # Remove built docs
  rm -rf docs/_build
fi


if command -v tree
then
  tree .
fi
